defmodule Freki.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add :owner_hash, :string
      add :char_id, :integer
      add :char_name, :string
      add :refresh_token, :string
      add :parent_id, references(:accounts, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:characters, [:owner_hash])
    create unique_index(:characters, [:char_id])
    create index(:characters, [:parent_id])
  end
end
