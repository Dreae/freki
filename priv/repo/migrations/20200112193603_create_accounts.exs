defmodule Freki.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :owner_hash, :string
      add :char_id, :integer
      add :char_name, :string
      add :refresh_token, :string

      timestamps()
    end

    create unique_index(:accounts, [:owner_hash])
  end
end
