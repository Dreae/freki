defmodule Freki.Repo.Migrations.CreateAuthTokens do
  use Ecto.Migration

  def change do
    create table(:auth_tokens) do
      add :access_token, :string
      add :expires, :utc_datetime
      add :char, references(:characters, on_delete: :nothing)

      timestamps()
    end

    create index(:auth_tokens, [:char])
  end
end
