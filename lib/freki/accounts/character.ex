defmodule Freki.Accounts.Character do
  use Ecto.Schema
  import Ecto.Changeset

  schema "characters" do
    field :char_id, :integer
    field :char_name, :string
    field :owner_hash, :string
    field :refresh_token, :string
    field :parent_id, :id

    timestamps()
  end

  @doc false
  def changeset(character, attrs) do
    character
    |> cast(attrs, [:owner_hash, :char_id, :char_name, :refresh_token])
    |> validate_required([:owner_hash, :char_id, :char_name, :refresh_token])
    |> unique_constraint(:owner_hash)
    |> unique_constraint(:char_id)
  end
end
