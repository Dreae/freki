defmodule Freki.Accounts.AuthToken do
  use Ecto.Schema
  import Ecto.Changeset

  schema "auth_tokens" do
    field :access_token, :string
    field :expires, :utc_datetime
    field :char, :id

    timestamps()
  end

  @doc false
  def changeset(auth_token, attrs) do
    auth_token
    |> cast(attrs, [:access_token, :expires])
    |> validate_required([:access_token, :expires])
  end
end
