defmodule Freki.Accounts.Account do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field :char_id, :integer
    field :char_name, :string
    field :owner_hash, :string
    field :refresh_token, :string

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:owner_hash, :char_id, :char_name, :refresh_token])
    |> validate_required([:owner_hash, :char_id, :char_name, :refresh_token])
    |> unique_constraint(:owner_hash)
  end
end
