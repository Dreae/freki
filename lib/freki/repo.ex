defmodule Freki.Repo do
  use Ecto.Repo,
    otp_app: :freki,
    adapter: Ecto.Adapters.Postgres
end
