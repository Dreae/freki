defmodule Freki.ESI do
  use HTTPoison.Base

  defstruct [:token, :client, :expires]
  
  alias __MODULE__
  alias Freki.Accounts
  alias Freki.Repo

  def create_oauth_client(access_token, refresh_token) do
    {:ok, client_id} = Keyword.fetch(Application.get_env(:freki, :frekiweb_oauth), :api_client_id)
    {:ok, client_secret} = Keyword.fetch(Application.get_env(:freki, :frekiweb_oauth), :api_client_secret)
    OAuth2.Client.new(
      client_id: client_id,
      client_secret: client_secret,
      site: "https://login.eveonline.com/",
      token: OAuth2.AccessToken.new(%{"access_token" => access_token, "refresh_token" => refresh_token})
    )
  end

  def create(%Accounts.AuthToken{char: char_id, access_token: token, expires: expires} = token) do
    char = Accounts.get_char!(char_id)
    client = create_oauth_client(token, char.refresh_token)

    %ESI{token: token, client: client, expires: DateTime.to_unix(expires)} |> maybe_refresh()
  end

  def maybe_refresh(%ESI{token: token, client: client, expires: expires} = esi) do
    now = DateTime.utc_now |> DateTime.to_unix
    if now > expires do
      {:ok, client} = OAuth2.Client.refresh_token(client)
      expires = OAuth2.AccessToken.expires_at(client.token)
      
      Accounts.AuthToken.changeset(token, %{access_token: client.token.access_token, expires: expires})
      |> Repo.update!()

      %{esi | client: client, expires: expires}
    else
      esi
    end
  end

  def process_request_url(url) do
    "https://esi.evetech.net/latest" <> url
  end
end
