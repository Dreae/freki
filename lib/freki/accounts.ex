defmodule Freki.Accounts do
  import Ecto.Query
  alias Freki.Repo

  def search_owner_hash(hash) do
    Repo.one(from u in Freki.Accounts.Account, where: u.owner_hash ==  ^hash)
  end

  def create(char_id, owner_hash, char_name, refresh_token) when is_integer(char_id) do
    %Freki.Accounts.Account{
      char_id: char_id,
      owner_hash: owner_hash,
      char_name: char_name,
      refresh_token: refresh_token
    }
    |> Repo.insert()
  end

  def get_char!(char_id) do
    Repo.get!(Freki.Accounts.Character, char_id)
  end
end
