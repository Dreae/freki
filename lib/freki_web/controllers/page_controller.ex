defmodule FrekiWeb.PageController do
  use FrekiWeb, :controller

  alias Freki.Accounts

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def oauth_client(conn, state \\ nil) do
    {:ok, client_id} = Keyword.fetch(Application.get_env(:freki, :frekiweb_oauth), :account_client_id)
    {:ok, client_secret} = Keyword.fetch(Application.get_env(:freki, :frekiweb_oauth), :account_client_secret)
    OAuth2.Client.new(
      client_id: client_id,
      client_secret: client_secret,
      site: "https://login.eveonline.com/",
      params: %{scope: "publicData", state: state},
      redirect_uri: Routes.page_url(conn, :oauth_callback)
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  def oauth_create(conn, _params) do
    state = Base.encode16(:crypto.strong_rand_bytes(16))
    put_session(conn, :oauth_state, state)
    |> redirect(external: OAuth2.Client.authorize_url!(oauth_client(conn, state)))
  end

  def oauth_callback(conn, %{"code" => code, "state" => state}) do
    session_state = get_session(conn, :oauth_state)

    if state !== session_state do
      redirect(conn, to: "/")
    else
      client = oauth_client(conn)
      |> OAuth2.Client.get_token!(code: code)

      {:ok, response} = HTTPoison.get("https://esi.evetech.net/verify/", [
        "Authorization": "#{client.token.token_type} #{client.token.access_token}",
        "Accept": "application/json"
      ])

      %{
        "CharacterName" => char_name,
        "CharacterOwnerHash" => owner_hash,
        "CharacterID" => char_id
      } = Jason.decode!(response.body)

      account = Accounts.search_owner_hash(owner_hash)
      if account == nil do
        {:ok, account} = Accounts.create(char_id, owner_hash, char_name, client.token.refresh_token)
        put_session(conn, :account, account) |> redirect(to: "/")
      else
        put_session(conn, :account, account) |> redirect(to: "/")
      end
    end
  end

  def logout(conn, %{"logout" => "true"}) do
    put_session(conn, :account, nil) |> redirect(to: "/")
  end
end
