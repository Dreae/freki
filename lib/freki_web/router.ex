defmodule FrekiWeb.Router do
  use FrekiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_user_account
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FrekiWeb do
    pipe_through :browser

    get "/", PageController, :index
    post "/logout", PageController, :logout
    get "/oauth/login", PageController, :oauth_create
    get "/oauth/callback", PageController, :oauth_callback
  end

  # Other scopes may use custom stacks.
  # scope "/api", FrekiWeb do
  #   pipe_through :api
  # end
  def fetch_user_account(conn, _) do
    assign(conn, :account, get_session(conn, :account))
  end
end
