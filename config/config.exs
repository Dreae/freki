# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :freki,
  ecto_repos: [Freki.Repo]

# Configures the endpoint
config :freki, FrekiWeb.Endpoint,
  url: [host: "freki.local"],
  secret_key_base: "nRWP6hYHoqTyw7z2YDv20loOnGmwFOgYPOJy58BxlP5Mf8RwApxau5kqoz84Jaju",
  render_errors: [view: FrekiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Freki.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
